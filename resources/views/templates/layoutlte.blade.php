<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from sgi2.binara.com.ar/vistas/inicio.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 06 Dec 2018 01:10:37 GMT -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Sistema de Gestión Interno</title>

	<!-- Global stylesheets -->
	<!--link href="../../fonts.googleapis.com/css1381.css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"-->
	<link href={{asset("global_assets/css/icons/icomoon/styles.css")}} rel="stylesheet" type="text/css">
	<link href={{asset("assets/css/bootstrap.min.css")}} rel="stylesheet" type="text/css">
	<link href={{asset("assets/css/bootstrap_limitless.min.css")}} rel="stylesheet" type="text/css">
	<link href={{asset("assets/css/layout.min.css")}} rel="stylesheet" type="text/css">
	<link href={{asset("assets/css/components.min.css")}} rel="stylesheet" type="text/css">
	<link href={{asset("assets/css/colors.min.css")}} rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	<!--mensajria-->
    <link href={{asset("css/sweetalert.css")}} rel="stylesheet" type="text/css">
    <!-- css calendario bootstrap -->
  	<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}">
  	
	

	

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark bg-indigo">
		<div class="navbar-brand wmin-0 mr-5">
			<a href="index.html" class="d-inline-block">
				<img src="{{asset('img/logo.gif')}}" alt="cargando..">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">

			</ul>
			<!--
			<span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto">16 orders</span>
			-->

			<ul class="navbar-nav ml-auto">
				<!--
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<img src="global_assets/images/lang/gb.png" class="img-flag mr-2" alt="">
						English
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a href="#" class="dropdown-item english"><img src="global_assets/images/lang/gb.png" class="img-flag" alt=""> English</a>
						<a href="#" class="dropdown-item ukrainian"><img src="global_assets/images/lang/ua.png" class="img-flag" alt=""> Українська</a>
						<a href="#" class="dropdown-item deutsch"><img src="global_assets/images/lang/de.png" class="img-flag" alt=""> Deutsch</a>
						<a href="#" class="dropdown-item espana"><img src="global_assets/images/lang/es.png" class="img-flag" alt=""> España</a>
						<a href="#" class="dropdown-item russian"><img src="global_assets/images/lang/ru.png" class="img-flag" alt=""> Русский</a>
					</div>
				</li>
				-->

				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link" title="Tiene nuevas notificaciones">
						<i class="icon-bell2"></i>
						<span class="d-xl-none ml-2">Notificaciones</span>
						<span class="badge badge-mark border-pink ml-auto ml-md-0"></span>
					</a>					
				</li>

				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						<img src="{{asset('img/avatar.png')}}" class="rounded-circle mr-2 img-responsive" height="34" alt="">
						<span>{{ Session::get('usr')}}</span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a href="#" class="dropdown-item"><i class="icon-user-plus"></i> Mi perfil</a>
						<a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Mensajes <span class="badge badge-pill bg-blue ml-auto">8</span></a>
						<div class="dropdown-divider"></div>
						<a href="{{url('logout')}}" class="dropdown-item"><i class="icon-exit3"></i> Finalizar</a>
					</div>

				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Secondary navbar -->
	<div class="navbar navbar-expand-md navbar-light">
		<div class="text-center d-md-none w-100">
			<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
				<i class="icon-unfold mr-2"></i>
				Menú
			</button>
		</div>

		<div class="navbar-collapse collapse" id="navbar-navigation">
			<ul class="navbar-nav navbar-nav-highlight">
				<li class="nav-item">
					<a href="index.html" class="navbar-nav-link active">
						<i class="icon-home4 mr-2"></i>
						Inicio
					</a>
				</li>

				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<i class="icon-office mr-2"></i>
						Agence
					</a>

					<div class="dropdown-menu">
						<!--<div class="dropdown-header">Basic layouts</div>-->
						<a href="nueva_empresa_exportadora.html" class="dropdown-item"><i class="icon-file-plus"></i> Nueva Agence</a>
						<a href="#" class="dropdown-item"><i class="icon-file-spreadsheet"></i> Listar Agence</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<i class="icon-list2 mr-2"></i>
						Projetos
					</a>

					<div class="dropdown-menu">
						<!--<div class="dropdown-header">Basic layouts</div>-->
						<a href="#" class="dropdown-item"><i class="icon-file-plus"></i> Nuevo Projetos</a>
						<a href="#" class="dropdown-item"><i class="icon-file-spreadsheet"></i> Listar Projetos</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<i class="icon-calendar mr-2"></i>
						Administrativo
					</a>

					<div class="dropdown-menu">
						<a href="#" class="dropdown-item"><i class="icon-megaphone"></i> Nueva campaña</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<i class="icon-cart mr-2"></i>
						Comercial
					</a>

					<div class="dropdown-menu">
						<!--<div class="dropdown-header">Basic layouts</div>-->
						<a href="#" class="dropdown-item"><i class="icon-file-stats"></i> Performance Comercial
						</a>
						
					</div>
				</li>

				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<i class="icon-stats-dots mr-2"></i>
						Financeiro
					</a>

					<div class="dropdown-menu">
						<a href="#" class="dropdown-item"><i class="icon-file-stats"></i> 
							Indefinido
						</a>
					</div>
				</li>

			</ul>

			<!--
			<ul class="navbar-nav navbar-nav-highlight ml-md-auto">
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<i class="icon-make-group mr-2"></i>
						Connect
					</a>

					<div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
						<div class="dropdown-content-body p-2">
							<div class="row no-gutters">
								<div class="col-12 col-sm-4">
									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-github4 icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">Github</div>
									</a>

									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-dropbox text-blue-400 icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">Dropbox</div>
									</a>
								</div>
								
								<div class="col-12 col-sm-4">
									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-dribbble3 text-pink-400 icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">Dribbble</div>
									</a>

									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-google-drive text-success-400 icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">Drive</div>
									</a>
								</div>

								<div class="col-12 col-sm-4">
									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-twitter text-info-400 icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">Twitter</div>
									</a>

									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-youtube text-danger icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">Youtube</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</li>

				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<i class="icon-cog3"></i>
						<span class="d-md-none ml-2">Settings</span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
						<a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
						<a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
						<div class="dropdown-divider"></div>
						<a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
					</div>
				</li>
			</ul>
			-->
		</div>
	</div>
	<!-- /secondary navbar -->


	<!-- Page header -->
	<!--
	<div class="page-header">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-dots mr-2"></i> <span class="font-weight-semibold">Tablero general</span></h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>

			<div class="header-elements d-none py-0 mb-3 mb-md-0">
				<div class="breadcrumb">
					<a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Inicio</a>
					<span class="breadcrumb-item active">Tablero general</span>
				</div>
			</div>
		</div>
	</div>
	-->
	<!-- /page header -->
		

	<!-- Page content -->
	<div class="page-content pt-0">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content">
				@yield('content')
				@include('flash::message')

			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						<!--Footer-->
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						<!--
						&copy; 2015 - 2018. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
						-->
					</span>

					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item"><a href="#" class="navbar-nav-link"><i class="icon-lifebuoy mr-2"></i> Mesa de ayuda</a></li>
						<!--
						<li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a></li>
						<li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>
						-->
					</ul>
				</div>
			</div>
			<!-- /footer -->
	<!-- Core JS files -->
	<!--script src={{asset("global_assets/js/main/jquery.min.js")}}></script-->
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<script src={{asset("global_assets/js/main/bootstrap.bundle.min.js")}}></script>
	<script src={{asset("global_assets/js/plugins/loaders/blockui.min.js")}}></script>
	<script src={{asset("global_assets/js/plugins/ui/slinky.min.js")}}></script>
	<script src={{asset("global_assets/js/plugins/ui/ripple.min.js")}}></script>
	<!-- /core JS files -->
	<!-- js calendario bootstrap -->
	<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('js/locales/bootstrap-datepicker.es.min.js')}}"></script>
	<!-- mensajeria -->
    <script src={{asset("js/sweetalert.js")}}></script>
    <!--script generales-->
    <script src={{asset("js/script.js")}}></script>
	<!-- Theme JS files -->
	<script src={{asset("global_assets/js/plugins/visualization/d3/d3.min.js")}}></script>
	<script src={{asset("global_assets/js/plugins/visualization/d3/d3_tooltip.js")}}></script>
	<script src={{asset("global_assets/js/plugins/forms/styling/switchery.min.js")}}></script>
	<script src={{asset("global_assets/js/plugins/forms/selects/bootstrap_multiselect.js")}}></script>
	<script src={{asset("global_assets/js/plugins/ui/moment/moment.min.js")}}></script>
	<script src={{asset("global_assets/js/plugins/pickers/daterangepicker.js")}}></script>

	<script src={{asset("assets/js/app.js")}}></script>
	<script src={{asset("global_assets/js/demo_pages/dashboard.js")}}></script>
	<!-- /theme JS files -->



	<!-- Theme JS files -->
	<script src={{asset("global_assets/js/plugins/forms/inputs/inputmask.js")}}></script>
	<script src={{asset("global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js")}}></script>
	<script src={{asset("global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js")}}></script>
	<script src={{asset("global_assets/js/plugins/uploaders/fileinput/fileinput.min.js")}}></script>
	<script src={{asset("global_assets/js/plugins/extensions/jquery_ui/interactions.min.js")}}></script>
	<script src={{asset("global_assets/js/plugins/forms/selects/select2.min.js")}}></script>
	<script src={{asset("assets/js/app.js")}}></script>
	
	<script src={{asset("global_assets/js/demo_pages/form_select2.js")}}></script>
	<script src={{asset("global_assets/js/demo_pages/uploader_bootstrap.js")}}></script>
	<!-- /theme JS files -->

	
	<!-- Include this after the sweet alert js file -->
    @include('sweet::alert')
</body>

<!-- Mirrored from sgi2.binara.com.ar/vistas/inicio.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 06 Dec 2018 01:11:15 GMT -->
</html>
