@extends('templates/layoutlte')
@section('content')

	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4>
					<i class="icon-dots mr-2"></i> 
					<span class="font-weight-semibold">{{$title}}</span>
				</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none">
					<i class="icon-more"></i>
				</a>
			</div>

			<div class="header-elements d-none py-0 mb-3 mb-md-0">
				<div class="breadcrumb">
					<span class="breadcrumb-item active">
						<i class="icon-clipboard3 mr-2"></i><a href="#"></a>Por Consultores
					</span>
					<span class="breadcrumb-item ">
						<i class="icon-users2 mr-2"></i><a href="#"></a> Por Clientes
					</span>
				</div>
			</div>
		</div>
	</div>
	<!-- /page header -->
<table>
	<tr><td colspan=""></td></tr>
</table>
	<div class="row">
		<div class="col-md-12">

			<!-- Basic layout-->
			<div class="card">
				<div class="card-header header-elements-inline">
				
					<h5 class="card-title">{{$descripcion}}:</h5>
					<div class="header-elements">
						<div class="list-icons">
	                		<a class="list-icons-item" data-action="reload"></a>
	                	</div>
                	</div>
				
				</div>
				
				<div class="card-body">

					<form id="comercial"> 

						<div class="row d-flex justify-content-around">
							<div class="col-md-8">
								<div class="form-group row">
									<label class="col-lg-3 col-form-label"><b>Periodo</b> <span class="text-danger">*</span></label>
									<div class="col-lg-9">
										<div class="input-daterange input-group" id="datepicker">

								          	<i class="icon-calendar"></i><span>&nbsp; </span><b> Desde:</b> 
								          	
								          	<input type="text" class="form-control" id="desde" name="desde" placeholder="Ingresar Fecha" />
								          	<i class="icon-calendar"></i><span>&nbsp; </span><b> Hasta:</b> 
								          	<input type="text" class="form-control" id="hasta" name="hasta" placeholder="Ingresar Fecha" />
								          
								        </div>
								        <div class="input-group date">
											 
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label"><b>Consultores</b> <span class="text-danger">*</span></label>
									<div class="col-lg-9"><span><i class="icon-user"></i></span>
		                            <select data-placeholder="Seleccione Consultores..." id="co_usuario" name="co_usuario[]" class="form-control select-search" multiple="multiple" data-fouc required>
		                            	@foreach ($listaConsultores as $key => $consultores) 
		                            	
											<option value="">Seleccione un Consultor</option>
											
											<option value="{{ @$consultores->rCousuario->co_usuario }}">{{ @$consultores->rCousuario->no_usuario }}</option>
										
										@endforeach 
										<span><i class="icon-user"></i></span>
		                            </select>
									</div>
								</div>							
							</div>
							<div class="col-md-2">
								<div class="text-center">									
									<a href="#" class="btn btn-primary form-control" id="relatorio" onclick="Relatorio('comercial');"> Relatório</a>
									<br><br>
									<a href="#" id="barra" class="btn btn-success form-control" id="grafico1" onclick="GraficoBarras('comercial');"> Grafico</a>
									<br><br>
									<a href="#" id="dona" class="btn btn-warning form-control" onclick="GraficoDone('comercial');"> Pizza</a>
								</div>
							</div>
							
						</div>
						<br><br>
						<!-- Seccion relatorio -->
						<div class="row">
							<div class="col-md-12" id="resultado">
								<!-- imprimiendo resultado -->
							</div>
						</div>

						<!-- fin sccion relatorio-->							
	
						<br><br>
						<div class="row">
							
							<div class="col-md-12" id="graficobarra1" style="display: none;">
								<canvas id="bar-chart-grouped" width="600" height="350"></canvas>
							</div>
							<div class="col-md-12" id="graficodona" style="display: none;">
								<canvas id="pie-chart" width="600" height="350"></canvas>
							
							</div>
							
						</div>	

					</form>
				</div>
			</div>
		</div>
	</div>
@stop
<!-- Resources -->


<script src="{{asset('js/Chart.min.js')}}"></script>




<style type="text/css">
	#chartdiv {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}		
</style>

<!-- Chart code -->

<script type="text/javascript">


function GraficoBarras(idForm) {
	
	if ($("#desde").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Desde', 'warning',{ button: "Ok!",});
                $("#desde").focus();
	}
	if ($("#hasta").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Hasta', 'warning',{ button: "Ok!",});
                $("#hasta").focus();
	}
	if ($("#co_usuario").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe Seleccionar un consultor', 'warning',{ button: "Ok!",});
                $("#co_usuario").focus();
	}
	$('#graficobarra1').show(1000).animate({width: "show"}, 1000,"linear");
  	$('#resultado,#graficodona').hide(1000).animate({height: "hide"}, 1000,"linear");
    var formData = $("#" + idForm).serialize(); 
    $.ajax({
        data: formData,
        type: "GET",
        url: "/admin/GraficoBarras",
        success: function (data) 
        {
        	
        	$.each(data, function(index, val) {
        		var neto=val.liquido;
      			var meses=val.mes;
      			var usuario= val.no_usuario;

      			//alert(neto);
				var UsrArray = new Array();
				UsrArray.push(usuario);
				var NetoArray = new Array();
				NetoArray.push(neto);
				
				
			});	
			
			
				
				//console.log(UsrArray);

      			new Chart(document.getElementById("bar-chart-grouped"), {
				    type: 'bar',
				    data: 
				    { 

						      labels: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre"],
						      datasets: 
								      [
								        {
								          label: UsrArray[0],
								          backgroundColor: "#ED8585",
								          data: NetoArray[0]
								        }, 
								        
								        {
								          label: UsrArray[1],
								          backgroundColor: "#51C15E",
								          data: NetoArray[1]
								        },
								        {
								          label: UsrArray[2],
								          backgroundColor: "#C1BF51",
								          data: NetoArray[2]
								        },
								        {
								          label: UsrArray[3],
								          backgroundColor: "#8e5ea2",
								          data: NetoArray[3]
								        },
								        {
								          label: UsrArray[4],
								          backgroundColor: "#51B3C1",
								          data: NetoArray[4]
								        }

								      ]

				    },
				    options: {
				      title: {
				        display: true,
				        text: 'Population growth (millions)'
				      }
				    }
				});
        	
        }
    });
    return false;
}

function GraficoDone(idForm) {
	if ($("#desde").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Desde', 'warning',{ button: "Ok!",});
                $("#desde").focus();
	}
	if ($("#hasta").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Hasta', 'warning',{ button: "Ok!",});
                $("#hasta").focus();
	}
	if ($("#co_usuario").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe Seleccionar un consultor', 'warning',{ button: "Ok!",});
                $("#co_usuario").focus();
	}
	$('#graficodona').show(1000).animate({width: "show"}, 1000,"linear");
  	$('#graficobarra1,#resultado').hide(1000).animate({height: "hide"}, 1000,"linear");
    var formData = $("#" + idForm).serialize(); 
    $.ajax({
        data: formData,
        type: "GET",
        url: "/admin/GraficoDone",
        success: function (data) {
        	$.each(data, function(index, val) {
        		var neto=val.liquido;
      			
      			var usuario= val.no_usuario;

      			//alert(neto);
				var UsrArray = new Array();
				UsrArray.push(usuario);
				var NetoArray = new Array();
				NetoArray.push(neto);
				
				
				
				
				
				console.log(UsrArray);
				//alert(UsrArray);
				/*for(var clave in UsrArray) {
				    alert(UsrArray['mes']);

				    if(UsrArray['mes']==1){
				    	var mes1='Enero';

				    	//alert(NetoArray);
				    }elseif(UsrArray['mes']==1){

				    }
			
				}*/
				
				new Chart(document.getElementById("pie-chart"), {
				    type: 'pie',
				    data: {
				      labels: UsrArray,
				      datasets: [{
				        label: "Population (millions)",
				        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
				        data: NetoArray
				      }]
				    },
				    options: {
				      title: {
				        display: true,
				        text: 'Predicted world population (millions) in 2050'
				      }
				    }
				});

			});	

	      
        }
    });
    return false;
}
function Relatorio(idForm) 
{
	if ($("#desde").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Desde', 'warning',{ button: "Ok!",});
                $("#desde").focus();
	}
	if ($("#hasta").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe ingresar un valor en el campo fecha Hasta', 'warning',{ button: "Ok!",});
                $("#hasta").focus();
	}
	if ($("#co_usuario").val() == "") {
		swal('Campo no puede estar vacio!', 'Debe Seleccionar un consultor', 'warning',{ button: "Ok!",});
                $("#co_usuario").focus();
	}

	$('#resultado').show(1000).animate({width: "show"}, 1000,"linear");
  	$('#graficobarra1,#graficodona').hide(1000).animate({height: "hide"}, 1000,"linear");
		var formData = $("#" + idForm).serialize();

		// paticion ajax
        var request = $.ajax({                        
					           type: "GET",                 
					           url: "/admin/comercial/relatorio",                     
					           data: formData,
					           //dataType: "html"
						      });
        // validando respuesta 
        request.done(function( data ) {
				
			  	$('#resultado').html(data)
		});

        // falla la respuesta
        request.fail(function( jqXHR, textStatus ) {
			  data="<div class='col-md-12 text-danger'> No se Encontro Resultados...</div>";
			  $('#resultado').html(data);
		});

	return false;
}
	
</script>